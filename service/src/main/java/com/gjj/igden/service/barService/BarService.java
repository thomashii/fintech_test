package com.gjj.igden.service.barService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gjj.igden.dao.BarDao;
import com.gjj.igden.dao.InstIdDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Bar;
import com.gjj.igden.service.util.ServiceException;

@Service
public class BarService {

    private  static final Logger logger = LoggerFactory.getLogger(BarService.class);
	
  @Autowired
  private BarDao barDao;

  @Autowired
  private InstIdDao instIdDaoImpl;

  public Set<Bar> getBarList(String instId) {
	  logger.debug("In market data service, Retrieving market data list for symbol::" + instId);

      List<Bar> result = barDao.getBarList(instId);
      return new HashSet<>(result);
  }

  public Bar getSingleBar(long barId, String instId) {
	  logger.debug("In market data service, Retrieving market data  for symbol::" + instId + barId);
    return barDao.getSingleBar(barId, instId);
  }

  public boolean update(Bar bar) {
	  logger.debug("In market data service, Updating market data  for:: " + bar);
    return barDao.updateBar(bar);
  }

  public boolean createBar(Bar bar) throws ServiceException {
	  logger.debug("In market data service, Creating market data  for ::" + bar);
    try {
      return barDao.createBar(bar);
    } catch (DAOException e) {
    	 logger.error("In market data service, Market data is no created ::" + e);
      throw new ServiceException.ExceptionBuilder().setException(e).build();
    }
  }

  public boolean deleteBar(Bar bar) throws DAOException {
	  logger.debug("In market data service, Deleting market data  for ::" + bar);
    return barDao.deleteBar(bar);
  }

  public List<String> searchTickersByChars(String tickerNamePart) {
	  logger.debug("In market data service, Search Tickers By Chars  ::" + tickerNamePart);
	  return instIdDaoImpl.searchTickersByChars(tickerNamePart.trim());
	  //return barDao.searchTickersByChars(tickerNamePart.trim());
  }
  
  public List<String> searchTickersByChars3(String tickerNamePart) {
	  logger.debug("In market data service, Search Tickers By Chars  ::" + tickerNamePart);
	  return barDao.searchTickersByChars(tickerNamePart.trim());
	  //return barDao.searchTickersByChars(tickerNamePart.trim());
  }
  
  public List<Bar> getFullTickersObjectByChars(String tickerNamePart) {
	  logger.debug("In market data service, Get Full Tickers Object By Chars ::" + tickerNamePart);
	    return barDao.getFullTickersObjectByChars(tickerNamePart.trim());
	  }
  
  public List<Bar> getAllBar() {
	  logger.debug("In market data service, Retrieving all market data ::");
	  return barDao.readAll();
  }
}
