package com.gjj.igden.service.test;

import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.accountService.AccountService;
import com.gjj.igden.service.passwordencoder.AppPasswordEncoder;
import com.gjj.igden.service.test.daostub.AccountDaoStub;
import com.gjj.igden.service.test.daostub.WatchListDescDaoStub;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;


@Configuration
@ComponentScan(basePackageClasses = {AccountService.class,
        AppPasswordEncoder.class, AccountDaoStub.class, WatchListDescDaoStub.class})
class SpringConfigContextAccountService {

    @Autowired
    private DataSource dataSource;

    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2);
        return builder.build();
    }
}

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigContextAccountService.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @Test
    public void test01simpleReadTest() {
        List<Account> accounts = accountService.getAccountList();
        accounts.forEach(System.out::println);
        //Assert.assertEquals("quant@account.com", accounts.get(0).getEmail());
        Assert.assertEquals("eMail_test1", accounts.get(0).getEmail());

    }

    @Test
    public void test02Create() {
        Account existingAccount = accountService.retrieveAccount(Long.valueOf(1));
        Assert.assertNotNull(existingAccount);
        try {
            Account account = new Account(existingAccount);
            account.setId(Long.valueOf(111));
            account.setAccountName(" ***** new Account ****** ");
            int oldSize = accountService.getAccountList().size();

            accountService.createAccount(account);
            int newSize = accountService.getAccountList().size();
            List<Account> accounts = accountService.getAccountList();
            accounts.forEach(System.out::println);
            Assert.assertNotEquals(oldSize, newSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
      public void test03simpleReadTest()  {
        List<Account> accounts = accountService.getAccountList();
        accounts.forEach(System.out::println);
        System.out.println("==========");
        System.out.println("==========");
        List<IWatchListDesc> watchListDescs = new ArrayList<>(1);
        accounts.stream()
          .filter(p -> p.getAccountName().equals("accountName_test2")).findAny()
          .ifPresent(p -> watchListDescs.addAll(p.getAttachedWatchedLists()));
        System.out.println(watchListDescs.size());
        Assert.assertEquals(1, watchListDescs.size());
            Assert.assertTrue(accounts.stream()
          .anyMatch(p -> p.getAccountName().equals("accountName_test1")));
      }

      @Test
      public void test04EditData()  {
        List<Account> accounts = accountService.getAccountList();
        accounts.stream().forEach((v) -> System.out.println(">>>>Account id: " + v.getId()));

        Account account = accountService.retrieveAccount(Long.valueOf(111));
        String oldName = account.getAccountName();
        account.setAccountName("test update");
        accountService.updateAccount(account);

        String newName = accountService.retrieveAccount(Long.valueOf(111)).getAccountName();
        Assert.assertNotEquals(newName, oldName);
      }

      @Test
      public void test05SimpleDelete()  {
        int oldSize = accountService.getAccountList().size();
        try {
            accountService.delete(Long.valueOf(111));
            int newSize = accountService.getAccountList().size();
            Assert.assertNotEquals(oldSize, newSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
      }
}

