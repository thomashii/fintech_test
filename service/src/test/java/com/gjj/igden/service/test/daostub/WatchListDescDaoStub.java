package com.gjj.igden.service.test.daostub;

import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListDescKey;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@SuppressWarnings("unlikely-arg-type")
public class WatchListDescDaoStub implements WatchListDescDao {
  private static Map<Long, List<IWatchListDesc>> watchListDescsDb;

  static {
    List<IWatchListDesc> watchListDescsAttachedToAccWithIdOne = Stream.of(
            new WatchListDesc(), new WatchListDesc(),
            new WatchListDesc(), new WatchListDesc()).collect(Collectors.toList());

    WatchListDesc theWatchListD = new WatchListDesc();
    theWatchListD.setWatchListName("test-aapl-5minBar-preMarketdata");

    List<IWatchListDesc> watchListDescsAttachedToAccWithIdTwo = Lists.newArrayList(theWatchListD);

    watchListDescsDb = Maps.newHashMap(
            ImmutableMap.of(1L, watchListDescsAttachedToAccWithIdOne,
        2L, watchListDescsAttachedToAccWithIdTwo));
  }

  public List<String> getAllStockSymbols(Long id) {
    return Stream.of("C@NYSE", "GS@NYSE").collect(Collectors.toList());
  }

  @Override
  public List<String> getAllStockSymbols(Long dsId, Long accId) {
        return null;
    }

  @Override
  public List<IWatchListDesc> getDataSetsAttachedToAcc(Long id) {
    return watchListDescsDb.get(id);
  }

  @Override
  public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
  }

  @Override
  public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
    return watchListDescsDb.get(accId).get(dsId.intValue());
  }

  @Override
  public boolean addTicker(Long accId, Long watchlistId, String instId) {
        return false;
    }

  public boolean addTicker(Long watchlistId, String tickerName) {
    return true;
  }

  @Override
  public boolean deleteWatchListDesc(Long dsId, Long accId) {
    return false;
  }



  @Override
  public boolean deleteWatchListDesc(IWatchListDesc watchListDesc) {
    return watchListDescsDb.entrySet()
      .stream()
      .anyMatch(e -> e.getValue()
        .removeIf(p -> p.equals(watchListDesc)));
  }

    @Override
    public boolean createWatchListDesc(IWatchListDesc watchListDesc) {
        return false;
    }

    @Override
    public boolean updateWatchListDesc(IWatchListDesc watchListDesc) {
        return false;
    }

    @Override
    public boolean createWatchListDesc(WatchListDesc watchListDesc) {
        return watchListDescsDb.get(watchListDesc.getAccount()).add(watchListDesc);
    }

  
    //@Override
    public boolean updateWatchListDesc(WatchListDesc watchListDesc) {
        watchListDescsDb.get(watchListDesc.getAccount()).stream()
          .filter(a -> a.equals(watchListDesc))
          .findFirst()
          .ifPresent(p -> p.setWatchListName(watchListDesc.getWatchListName()));
        return true;
    }

    @Override
    public IWatchListDesc readByWatchListIdAndAccountId(Long watchListId, Long accountId) {
        return null;
    }

    @Override
    public IWatchListDesc read(IWatchListDesc obj) {
        return null;
    }

    @Override
    public List<IWatchListDesc> readAll() {
        return null;
    }

    @Override
    public boolean deleteWatchList(IWatchListDesc obj) throws DAOException {
        return false;
    }

    @Override
    public IWatchListDesc read(Long id, Long accId) {
        return null;
    }

    @Override
    public IWatchListDesc read(WatchListDescKey key) {
        return null;
    }

    @Override
    public IWatchListDesc read(Long id) {
        return null;
    }

    @Override
    public Long getMaxIdFromDatabase() {
        return null;
    }
}
