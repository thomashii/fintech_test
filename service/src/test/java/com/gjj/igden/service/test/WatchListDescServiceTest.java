package com.gjj.igden.service.test;

import java.util.List;

import com.gjj.igden.service.test.daostub.InstIdDaoStub;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.test.daostub.WatchListDescDaoStub;
import com.gjj.igden.service.watchlist.WatchListDescService;

@Configuration
@ComponentScan(basePackageClasses = {WatchListDescService.class,
  WatchListDescDaoStub.class, InstIdDaoStub.class})
class SpringTextContext {
}

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTextContext.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WatchListDescServiceTest {

  @Autowired
  private WatchListDescService watchListDescService;

  @Test
  public void test03simpleReadTest() throws Exception {
         watchListDescService.getStockSymbolsList(1L).forEach(System.out::println);
  }

  @Test
  public void test01CreateH2DataBaseTest() {
    List<IWatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = 4;
    Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void test04ReturnBarList() {
    IWatchListDesc dataSet = watchListDescService.getDataSetsAttachedToAcc(2L).get(0);
    System.out.println(dataSet.getWatchListName());
    Assert.assertNotNull(dataSet);
    Assert.assertEquals("test-aapl-5minBar-preMarketdata", dataSet.getWatchListName());
  }

  @Test
  public void test07Delete() throws Exception {
    List<IWatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = dataSetList.size();

    IWatchListDesc watchList = dataSetList.get(0);
    boolean deleteResultFlag = watchListDescService.delete(watchList.getWatchListDescKey().getWatchListId(),
            watchList.getWatchListDescKey().getAccountId());
    Assert.assertTrue(deleteResultFlag);
    System.out.println("after deletion ");
    dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    Assert.assertNotEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void test05CreateDataSet() throws Exception {
    Long accId = 1L;
    IWatchListDesc newWatchList = watchListDescService.getWatchListDesc(Long.valueOf(1L), accId);
    List<IWatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    int expectedDataSetsAmountAfterDeletion = 4;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
    Assert.assertNotNull(newWatchList);
    newWatchList.setId(111L);
    Account account = new Account();
    account.setId(accId);

    newWatchList.setAccount(account);
    newWatchList.setWatchListName("just testing around");
    Assert.assertTrue(watchListDescService.create(newWatchList, accId));
    dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    expectedDataSetsAmountAfterDeletion = 5;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
  }

  @Test
  public void test06Update() throws Exception {
    final Long accId = 1L;
    IWatchListDesc dataSet = watchListDescService.getWatchListDesc(1L, accId);
    dataSet.setWatchListName("test update");
    Account account = new Account();
    account.setId(accId);
    dataSet.setAccount(account);
    watchListDescService.update(dataSet);
    final String dataSetNameDirect = watchListDescService.getWatchListDesc(1L, 1L).getWatchListName();
    Assert.assertEquals("test update", dataSetNameDirect);
  }

  @Test
  public void test02Read() throws Exception {
    List<IWatchListDesc> watchListDescs = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int size = 4;
    Assert.assertEquals(size,
      watchListDescs.size());
  }

}
