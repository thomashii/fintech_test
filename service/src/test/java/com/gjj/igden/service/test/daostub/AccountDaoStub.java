package com.gjj.igden.service.test.daostub;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.accountService.AccountService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class AccountDaoStub implements AccountDao {

    private static final Logger logger = LoggerFactory.getLogger(AccountDaoStub.class);

    private static Map<Long, Account> accountDbSimulator;

  static {
    accountDbSimulator = Maps.newHashMap(ImmutableMap
      .of(1L, new Account(1L, "accountName_test1", "eMail_test1",
          "additionalInfo_test1",
          "password_test1", Stream.of(new WatchListDesc(), new WatchListDesc(),
        new WatchListDesc(), new WatchListDesc()).collect(Collectors.toList()),
          new Date()),
        2L, new Account(2L, "accountName_test2", "eMail_test2",
          "additionalInfo_test2",
          "password_test2", Stream.of(new WatchListDesc()).collect(Collectors.toList()),
          new Date())));
  }

  public void setDataSource(DataSource dataSource) {
  }

  public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
  }

  public List<Account> getAllAccounts() {
    //*/ return accountDbSimulator.entrySet().stream().collect(Collectors.toList());
    //**// return accountDbSimulator.values().stream().collect(Collectors.toList());
    //   return new ArrayList<>(accountDbSimulator.values());

      logger.debug(">>>Called getAllAccounts()");
      return accountDbSimulator.values().stream().collect(Collectors.toList());

      /*
    return accountDbSimulator.values()
      .stream()
      .filter(Objects::nonNull)
      .collect(Collectors.toList());
      */
  }

  public boolean delete(Account account) {
    return delete(account.getId());
  }

  public boolean delete(Long id) {
    return (accountDbSimulator.remove(id) != null);
  }

  public Account getAccountById(Long id) {
    return accountDbSimulator.get(1);
  }

  @Override
  public void update(Account acc) {
      updateAccount(acc);
  }

  public boolean updateAccount(Account acc) {
    return accountDbSimulator.put(acc.getId(), acc) == acc;
  }

  @Override
  public boolean deleteAccount(Account account) {
    return false;
  }

  @Override
  public void create(Account account) throws DAOException {
      accountDbSimulator.putIfAbsent(account.getId(), account);
  }

  public boolean createAccount(Account account) {
    // return accountDbSimulator.entrySet().stream().max((e1,e2)->e1.getKey())
    // return accountDbSimulator.Co().stream().max((e1,e2)->e1.getKey())
    long biggestKeyValue = Collections
      .max(accountDbSimulator.entrySet(), Map.Entry.comparingByKey()).getKey();
    return accountDbSimulator.put(++biggestKeyValue, account) == account;
  }

  @Override
  public boolean setImage(Long accId, InputStream is) {
    return true;
  }

  @Override
  public byte[] getImage(Long accId) {
    return new byte[0];
  }

  @Override
  public Account readByAccountName(Account account) {
    return null;
  }

  @Override
  public Account readByAccountName(String accountName) {
    return null;
  }

  @Override
  public List<Account> readAll() {
      return accountDbSimulator.values()
              .stream()
              .filter(Objects::nonNull)
              .collect(Collectors.toList());
  }

  @Override
  public Account read(Account account) {
    return accountDbSimulator.get(account.getId());
  }
}