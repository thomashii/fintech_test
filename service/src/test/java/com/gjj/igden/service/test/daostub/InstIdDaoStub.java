package com.gjj.igden.service.test.daostub;

import com.gjj.igden.dao.InstIdDao;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.InstId;
import com.gjj.igden.model.InstIdKey;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InstIdDaoStub implements InstIdDao {

    @Override
    public void create(InstId instId) throws DAOException {

    }

    @Override
    public InstId read(InstId obj) {
        return null;
    }

    @Override
    public InstId read(InstIdKey idKey) {
        return null;
    }

    @Override
    public List<InstId> readAll() {
        return null;
    }

    @Override
    public boolean delete(InstId instId) throws DAOException {
        return false;
    }

    @Override
    public void delete(String instId) {

    }

    @Override
    public void delete(Long watchListId, Long accountId) {

    }

    @Override
    public void updateInstIdToNull(long watchListId_fk) {

    }

    @Override
    public List<String> getAllStockSymbols(IWatchListDesc watchListDesc) {
        return null;
    }

    @Override
    public List<String> searchTickersByChars(String tickerNamePart) {
        return null;
    }
}
