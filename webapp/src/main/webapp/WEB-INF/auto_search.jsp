<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Autocomplete - Remote datasource</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--   <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $(function () {
	    var getData = function (request, response) {
	        $.getJSON(
	            "http://gd.geobytes.com/AutoCompleteCity?callback=?&q=" + request.term,
	            function (data) {
	            	console.log(data);
	                response(data);
	            });
	    };
	    
	    var searchAjax = function( request, response) {
			var data = {}
			data["searchParam"] = request.term;
			$.ajax({
				type : "GET",
				contentType : "application/json;charset=utf-8",
				url : "${home}/webapp/search_result?",
				data : data,
				dataType : 'json',
				processData: true,
				timeout : 100000,
				success : function(ajaxresponse) {
					console.log("SUCCESS: ", ajaxresponse);
					var instlist = ajaxresponse.item;
					response(instlist);
					console.log(instlist);
				},
				error : function(e) {
					console.log("ERROR--: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
	    };
	 
	    var selectItem = function (event, ui) {
	        $("#myText").val(ui.item.value);
	        return false;
	    }
	 
	    $("#myText").autocomplete({
	        source: searchAjax,
	        select: selectItem,
	        minLength: 1,
	        change: function() {
	            $("#myText").val("");
	        }
	    });
	});
 
  </script>
</head>
<body>
 
<div class="ui-widget">
  <label for="myText">Search: </label>
  <input id="myText" name ="myText">
</div>
 
<div class="ui-widget" style="margin-top:2em; font-family:Arial">
  Result:
  <div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content"></div> 
  


 
 
</body>
</html>