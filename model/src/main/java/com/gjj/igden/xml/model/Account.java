package com.gjj.igden.xml.model;

import com.gjj.igden.model.IAccount;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.Role;
import com.gjj.igden.model.WatchListDesc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account implements IAccount {
	@XmlAttribute()
	private Long id;
	private String accountName;
	private String email;
	private String additionalInfo;
	private String password;
	private String creationDate;

	public Account() {
	}
	
	public Account(com.gjj.igden.model.Account ac) {
		this.id = ac.getId();
		this.accountName = ac.getAccountName();
		this.email = ac.getEmail();
		this.additionalInfo = ac.getAdditionalInfo();
		this.password = ac.getPassword();
	}

	public Account(Long id, String accountName, String email, String additionalInfo, String password,
			String creationDate) {
		super();
		this.id = id;
		this.accountName = accountName;
		this.email = email;
		this.additionalInfo = additionalInfo;
		this.password = password;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Override
	public List<IWatchListDesc> getAttachedWatchedLists() {
		return null;
	}

	@Override
	public void setDescriptions(List<WatchListDesc> descList) {

	}

	@Override
	public List<IWatchListDesc> getDataSets() {
		return null;
	}

	@Override
	public void setDataSets(List<IWatchListDesc> dataSets) {

	}

	@Override
	public List<WatchListDesc> getDescriptions() {
		return null;
	}

	@Override
	public Set<Role> getRoles() {
		return null;
	}

	@Override
	public void setRoles(Set<Role> roles) {

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public void addRole(Role role) {

	}

	@Override
	public byte[] getAvatar() {
		return new byte[0];
	}

	@Override
	public void setAvatar(byte[] avatar) {

	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void setCreationDate(Date creationDate) {
		try {
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			this.creationDate = formatter.format(this.creationDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Date getCreationDate() {
		try {
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			return formatter.parse(this.creationDate);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Account [id=");
		builder.append(id);
		builder.append(", accountName=");
		builder.append(accountName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", additionalInfo=");
		builder.append(additionalInfo);
		builder.append(", password=");
		builder.append(password);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append("]");
		return builder.toString();
	}

}
