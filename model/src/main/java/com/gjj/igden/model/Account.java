package com.gjj.igden.model;

import java.util.*;
import java.util.stream.Collectors;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.gjj.igden.utils.EntityId;


@Entity
@Table(name = "account")
public class Account implements UserDetails, EntityId, IAccount {

	private static final long serialVersionUID = -8703071584693304580L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private Long id;
	
	@Column(name = "account_name", unique = true)
    private String accountName;
	
	@Column(unique = true)
    private String email;
	
    @Column(name = "additional_info")
    private String additionalInfo;
    private String password;
    
    @OneToMany(mappedBy = "account", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<WatchListDesc> descriptions;
    
    @Column(name = "creation_date")
    private Date creationDate;
    
    private boolean enabled;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "account_roles",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "role")})
    private Set<Role> roles = new HashSet<>();
    
    @Lob
   	@Basic(fetch = FetchType.LAZY)
   	@Column(name="avatar",length=20971520)
    private byte[] avatar;
    
    @Transient
    private String saltSource = "admin";
    
    public String getSaltSource() {
		return saltSource;
	}

	public Account() {
    }

    public Account(Account acc) {
        this.accountName = acc.accountName;
        this.setEmail(acc.email);
        this.additionalInfo = acc.additionalInfo;
        this.password = acc.password;
        this.descriptions = acc.descriptions;
        this.creationDate = acc.creationDate;
        this.avatar = acc.avatar;
    }

    public Account(Long id) {
		this.id = id;
	}

	public Account(String accountName, String email, String additionalInfo, String password,
                   List<WatchListDesc> descriptions, Date creationDate,byte[] avatar) {
        this.accountName = accountName;
        this.setEmail(email);
        this.additionalInfo = additionalInfo;
        this.password = password;
        this.descriptions = descriptions;
        this.creationDate = creationDate;
        this.avatar = avatar;
    }

    public Account(Long id, String accountName, String email,
                   String additionalInfo, String password,
                   List<WatchListDesc> dataSets, Date creationDate) {
        this.id = id;
        this.accountName = accountName;
        this.setEmail(email);
        this.additionalInfo = additionalInfo;
        this.password = password;
        this.descriptions = dataSets;
        this.creationDate = creationDate;
    }

    public Account(String accountName, String email,
                   String additionalInfo) {
        this.accountName = accountName;
        this.setEmail(email);
        this.additionalInfo = additionalInfo;
    }

    public Account(Long id, String accountName, String email, String additionalInfo,
    		Date creationDate) {
        this.id = id;
        this.accountName = accountName;
        this.setEmail(email);
        this.additionalInfo = additionalInfo;
        this.creationDate = creationDate;
    }

/*  public Account(String accountName, String email, String additionalInfo,
                 List<WatchListDesc> dataSets) {
    this(null, accountName, email, additionalInfo, dataSets);
  }*/

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account(Long accountId, String accountName, String email, String additionalInfo) {
        this.id = accountId; this.accountName = accountName; this.email = email; this.additionalInfo = additionalInfo;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    @Override
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    @Override
    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }


    @Override
    public String getAccountName() {
        return accountName;
    }

    @Override
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Override
    @Transient
    public List<IWatchListDesc> getAttachedWatchedLists() {
        return getDataSets();
    }

    @Override
    public void setDescriptions(List<WatchListDesc> descList) {

        this.descriptions = descList;
    }

    @Override
    @Transient
    public List<IWatchListDesc> getDataSets() {
        return descriptions.stream().filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public void setDataSets(List<IWatchListDesc> dataSets) {
        if(dataSets == null) {
            this.descriptions = new ArrayList<>();
        } else {
            this.descriptions = Arrays.asList(dataSets.toArray(new WatchListDesc[0]));
        }
    }
    
    @Override
    public List<WatchListDesc> getDescriptions() {
		return descriptions;
	}
    
	@Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(getEmail(), accountName);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Account && ((Account) obj).getEmail().equals(this.getEmail()) &&
                Objects.equals(((Account) obj).accountName, this.accountName);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(" [id=");
        builder.append(id);
        builder.append(", accountName= ");
        builder.append(accountName);
        builder.append(", email= ");
        builder.append(getEmail());
        builder.append(", additionalInfo= ");
        builder.append(additionalInfo);
        builder.append(", data_containers Sets names= ");
        builder.append(descriptions);
        builder.append("]\n");
        return builder.toString();
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public Set<Role> getRoles() {
        return roles;
    }

    @Override
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    @Override
    @Transient
    public String getUsername() {
        return getAccountName();
    }


    @Override
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }


    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return isEnabled();
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return isEnabled();
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return isEnabled();
    }


    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public void addRole(Role role) {
    	this.roles.add(role);
    }

	@Override
    public byte[] getAvatar() {
		return avatar;
	}

	@Override
    public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}
    
	
    
}
