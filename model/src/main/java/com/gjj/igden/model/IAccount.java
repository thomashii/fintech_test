package com.gjj.igden.model;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface IAccount {

    Long getId();

    void setPassword(String password);

    Date getCreationDate();

    void setCreationDate(Date creationDate);

    String getAdditionalInfo();

    void setAdditionalInfo(String additionalInfo);

    String getAccountName();

    void setAccountName(String accountName);

    @Transient
    List<IWatchListDesc> getAttachedWatchedLists();

    void setDescriptions(List<WatchListDesc> descList);

    @Transient
    List<IWatchListDesc> getDataSets();

    void setDataSets(List<IWatchListDesc> dataSets);

    List<WatchListDesc> getDescriptions();

    Set<Role> getRoles();

    void setRoles(Set<Role> roles);

    String getEmail();

    void setEmail(String email);

    void addRole(Role role);

    byte[] getAvatar();

    void setAvatar(byte[] avatar);
}
