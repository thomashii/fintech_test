package com.gjj.igden.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gjj.igden.utils.EntityId;
import com.google.common.base.Objects;

import java.io.Serializable;

@Entity
@Table(name = "wl_tickers")
public class InstId implements EntityId, Serializable {

	private static final long serialVersionUID = 1L;

	@Transient
	private final static String SEPARATOR = "@";

	@EmbeddedId
	public InstIdKey instIdKey;

	@Transient
	private String symbol;

	@Transient
	private String exchId;

	@Transient
	private Exchange exchange;

	@ManyToOne
	@MapsId("account_fk_Id,watchlist_id")
	@JoinColumns({
		@JoinColumn(name = "account_fk_Id"),
		@JoinColumn(name = "watchlist_id")
	})
	private WatchListDesc watchListDesc;

	public InstId() {
		this.instIdKey = new InstIdKey();
	}

	public InstId(String str) {
		validateCorrectString(str);
		String[] tokens = str.split("@");
		symbol = tokens[0].toUpperCase();
		exchId = tokens[1].toUpperCase();
		instIdKey = new InstIdKey(symbol, exchId);
	}

	public InstId(InstIdKey instIdKey, String symbol, String exchId, Exchange exchange, WatchListDesc watchListDesc) {
		super();
		this.instIdKey = instIdKey;
		this.symbol = symbol;
		this.exchId = exchId;
		this.exchange = exchange;
		this.watchListDesc = watchListDesc;
		this.instIdKey = new InstIdKey(symbol, exchId);
	}

	public InstId(String symbol, String exchId) {
		this.symbol = symbol;
		this.exchId = exchId;
		this.instIdKey = new InstIdKey(symbol, exchId);
	}

	public Exchange getExchange() {
		if (java.util.Objects.equals(exchId, "NASDAQ")) {
			return Exchange.NASDAQ;
		}
		if (java.util.Objects.equals(exchId, "NYSE")) {
			return Exchange.NYSE;
		}
		return exchange;
	}

	private void validateCorrectString(String str) {
	}

	public String getExchId() {
		return exchId;
	}

	public String getSymbol() {
		return symbol;
	}

	public InstIdKey getInstIdKey() {
		return instIdKey;
	}

	public void setInstIdKey(InstIdKey instIdKey) {
		this.instIdKey = instIdKey;
	}

	public WatchListDesc getWatchListDesc() {
		return watchListDesc;
	}

	public void setWatchListDesc(WatchListDesc watchListDesc) {
		this.watchListDesc = watchListDesc;
		this.instIdKey.setWatchListDescId(watchListDesc.getWatchListId());
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public void setExchId(String exchId) {
		this.exchId = exchId;
	}
	
	public void setExchId(String symbol, String exchId) {
		this.exchId = symbol + "@" + exchId;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof InstId)) {
			return false;
		}
		InstId altInstId = (InstId) o;
		return Objects.equal(symbol, altInstId.symbol) && Objects.equal(exchId, altInstId.exchId);
	}

	public Exchange getExchange(String exchId) {
		if (exchId.equalsIgnoreCase("NYSE")) {
			return Exchange.NYSE;
		} else if (exchId.equalsIgnoreCase("nasdaq")) {
			return Exchange.NASDAQ;
		} else {
			throw new IllegalArgumentException("your exchange id is not correct");
		}
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(symbol, exchId);
	}

	@Override
	public String toString() {
		return instIdKey.getId();
	}
	
	public String geInsttId() {
		return this.instIdKey.getId();
	}

	public void setInstId(String id) {
		this.instIdKey.setId(id);
	}
	
	public Long getWatchListDescId() {
		return instIdKey.getWatchListDescId();
	}

	public void setWatchListDescId(Long watchListDescId) {
		this.instIdKey.setWatchListDescId(watchListDescId);
	}

	@Transient
	@Override
	public Long getId() {
		return null;
	}

	@Override
	public void setId(Long id) {
		
	}
	
	public Long getAccountId() {
		return this.instIdKey.getAccountId();
	}

	public void setAccountId(Long accountId) {
		this.instIdKey.setAccountId(accountId);
	}
}
