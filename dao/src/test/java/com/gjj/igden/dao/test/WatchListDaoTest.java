package com.gjj.igden.dao.test;

import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.IWatchListDesc;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;


/*
@Configuration
@ComponentScan(basePackageClasses = {WatchListDescDao.class,
  WatchListDescStub.class})
class WatchListDaoTestConfig {
}
*/

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:beans-cp.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WatchListDaoTest {

  @Autowired
  private WatchListDescDao watchListDescDao;

  @Before
  public void setUp() {
    EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
      .setType(EmbeddedDatabaseType.H2)
      .addScript("db-init-sql-script/init-db-fintech_wsH2_moreData.sql")
      .build();
    NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
    watchListDescDao.setNamedParamJbd(template);
  }

  @Test
  @SqlGroup({
          @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:queries-test-scripts/instId-create-after-test.sql")
  })
  public void test05AddTicker() {
    boolean resultFlag = watchListDescDao.addTicker(1L, 15L, "AAL@NASDAQ"); //"C@NASDAQ");
    Assert.assertTrue(resultFlag);
    List<String> list = watchListDescDao.getAllStockSymbols(15L, 1L);
    //Assert.assertNotNull(list);
    //Assert.assertEquals("AAL@NASDAQ", list.get(2));
  }

  @Test
  public void test04GetAllStockSymbols() {
    List<String> tickerList = watchListDescDao.getAllStockSymbols(2L, 1L);
    Assert.assertNotNull(tickerList);
    final int expectedDataSetsAmount = 18;
    System.out.println(tickerList);
    Assert.assertEquals(expectedDataSetsAmount, tickerList.size());
  }

  @Test
  public void test06Read() throws Exception {
    List<IWatchListDesc> watchListDescs = watchListDescDao.getDataSetsAttachedToAcc(1L);
    Assert.assertNotNull(watchListDescs);
    final int stockSymbolNumAttachedToWatchedList17th = 18;

    List<String> list = watchListDescs.get(1).getStockSymbolsList();
    Assert.assertNotNull(list);

    Assert.assertEquals(stockSymbolNumAttachedToWatchedList17th, list.size());
  }

  @Test
  public void test02GetDataSetsAttachedToAcc() {
    List<IWatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = 9;
    Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void test01ReturnBarList() {
    IWatchListDesc dataSet = watchListDescDao.getWatchListDesc(1L, 1L);
    System.out.println(dataSet.getWatchListName());
    Assert.assertNotNull(dataSet);
    Assert.assertEquals("test-aapl-5minBar-preMarketdata", dataSet.getWatchListName());
  }

  @Test
  @SqlGroup({
          @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:queries-test-scripts/watchlist-delete-before-test.sql")
  })
  public void test09Delete() throws Exception {
    List<IWatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = 10;
    System.out.println(" again ");
    dataSetList.forEach(p -> System.out.println(p.getId()));
    Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());

    IWatchListDesc record = dataSetList.stream()
            .filter((v) -> v.getWatchListId() == 10000)
            .findFirst().orElse(null);

    Assert.assertNotNull(record);
    boolean deleteResultFlag = watchListDescDao.deleteWatchListDesc(record);
    Assert.assertTrue(deleteResultFlag);

    System.out.println("after deletion ");
    dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmountAfterDeletion = 9;
    dataSetList.forEach(p -> System.out.println(p.getId()));
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
  }

  @Test
  @SqlGroup({
          @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:queries-test-scripts/watchlist-create-after-test.sql")
  })
  public void test03CreateDataSet() throws Exception {
    List<IWatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    dataSetList.forEach(p -> System.out.print(p.getId() + " ; "));

    int expectedDataSetsAmountBeforeCreation = 9;
    Assert.assertEquals(expectedDataSetsAmountBeforeCreation, dataSetList.size());

    IWatchListDesc dataSet = watchListDescDao.getWatchListDesc(1L, 1L);
    Assert.assertNotNull(dataSet);

    dataSet.setWatchListId(Long.valueOf(10001));
    //dataSet.setWatchListId(null);
    dataSet.setWatchListName("just testing around");
    watchListDescDao.createWatchListDesc(dataSet);

    dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    Assert.assertEquals(expectedDataSetsAmountBeforeCreation + 1, dataSetList.size());

  }

  @Test
  @SqlGroup({
          @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:queries-test-scripts/watchlist-update-after-test.sql")
  })
  public void test07UpdateDesc() throws Exception {
    IWatchListDesc dataSet = watchListDescDao.getWatchListDesc(1L, 1L);
    Assert.assertNotNull(dataSet);

    dataSet.setWatchListName("test update");
    watchListDescDao.updateWatchListDesc(dataSet);
    final String dataSetNameDirect = watchListDescDao.getWatchListDesc(1L, 1L).getWatchListName();
    Assert.assertEquals("test update", dataSetNameDirect);
  }
}