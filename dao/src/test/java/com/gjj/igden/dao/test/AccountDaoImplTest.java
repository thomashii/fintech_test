package com.gjj.igden.dao.test;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.dao.daoimpl.AccountDaoImpl;
import com.gjj.igden.model.Account;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPATestConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountDaoImplTest {

    @Autowired
    private AccountDao testAccountDaoImpl;

    @Before
    public void setUpInMemoryDataBaseForTests() {
        EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2) // toask: is this line  redundant  ?
                .addScript("db-init-sql-script/init-db-fintech_wsH2_moreData.sql")
                .build();
        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
        testAccountDaoImpl.setNamedParamJbd(template);
    }

    @Test
    public void test03CreateH2DataBaseTest() {
        List<Account> accounts = testAccountDaoImpl.getAllAccounts();
        Assert.assertNotNull(accounts);
    }

    @Test
    public void test01ReadAll() throws Exception {
        List<Account> accounts = testAccountDaoImpl.getAllAccounts();
        Assert.assertEquals(2, accounts.size());
    }

    @Test
    public void test04ReadById() throws Exception {
        Account account = testAccountDaoImpl.getAccountById(1L);
        Assert.assertNotNull(account);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:queries-test-scripts/account-before-test.sql")
    })
    public void test06Delete() throws Exception {
        List<Account> accounts = testAccountDaoImpl.getAllAccounts();
        int originalAmount = accounts.size();
        Account record =  accounts.stream().filter((v) -> v.getId() == 10000).findFirst().orElse(null);
        Assert.assertNotNull(record);

        testAccountDaoImpl.deleteAccount(record);
        accounts = testAccountDaoImpl.getAllAccounts();
        int afterDel = accounts.size();
        Assert.assertEquals(originalAmount, afterDel + 1);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:queries-test-scripts/account-before-test.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:queries-test-scripts/account-after-test.sql")
    })
    public void test05Update() throws Exception {
        Account accounts = testAccountDaoImpl.getAccountById(Long.valueOf(10000));
        String oldInfo = accounts.getAdditionalInfo();
        accounts.setAdditionalInfo("test update");
        testAccountDaoImpl.update(accounts);
        final String additionalInfo = testAccountDaoImpl.getAccountById(Long.valueOf(10000)).getAdditionalInfo();
        Assert.assertNotEquals(oldInfo, additionalInfo);
        Assert.assertEquals("test update", additionalInfo);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:queries-test-scripts/account-create-after-test.sql")
    })
    public void test02CreateAccount() throws Exception {
        Account account = new Account();
        account.setAccountName("new user");
        account.setEmail("new_user@test.com");
        account.setAdditionalInfo("test my test");
        account.setPassword("P@ssWord");
        boolean flag = testAccountDaoImpl.createAccount(account);
        System.out.println(flag);
        List<Account> accounts = testAccountDaoImpl.getAllAccounts();
        Stream<Account> newAccount = accounts.stream().filter(p -> Objects.equals(p.getAccountName(),
                "new user"));
        Stream<Account> allAccounts = accounts.stream().filter(p -> p.getId() > 0);
        List<Account> result = newAccount.collect(Collectors.toList());
        List<Account> testResult = allAccounts.collect(Collectors.toList());
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("new_user@test.com", result.get(0).getEmail());
        Assert.assertEquals(3, testResult.size());
    }
}
