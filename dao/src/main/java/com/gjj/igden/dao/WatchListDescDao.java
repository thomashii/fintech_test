package com.gjj.igden.dao;

import java.util.List;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListDescKey;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gjj.igden.model.IWatchListDesc;

public interface WatchListDescDao {
	
	List<String> getAllStockSymbols(Long dsId, Long accId);

	List<IWatchListDesc> getDataSetsAttachedToAcc(Long id);

	void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd);

	IWatchListDesc getWatchListDesc(Long dsId, Long accId);

	boolean addTicker(Long accId, Long watchlistId, String instId);

	boolean deleteWatchListDesc(Long dsId, Long accId);

	boolean deleteWatchListDesc(IWatchListDesc watchListDesc);

	boolean createWatchListDesc(IWatchListDesc watchListDesc);

	boolean updateWatchListDesc(IWatchListDesc watchListDesc);

	IWatchListDesc readByWatchListIdAndAccountId(Long watchListId, Long accountId);

	IWatchListDesc read(IWatchListDesc obj);

	List<IWatchListDesc> readAll();

	boolean deleteWatchList(IWatchListDesc obj) throws DAOException;

	IWatchListDesc read(Long id, Long accId);

	IWatchListDesc read(WatchListDescKey key);

    IWatchListDesc read(Long id);

    boolean createWatchListDesc(WatchListDesc watchListDesc);

	Long getMaxIdFromDatabase();

}
