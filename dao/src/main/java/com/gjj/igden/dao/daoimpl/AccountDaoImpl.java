package com.gjj.igden.dao.daoimpl;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Account;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class AccountDaoImpl extends AbstractDAO<Account> implements AccountDao {
	
	@Autowired
	WatchListDescDao watchListDescDaoImpl;

    @Override
    public boolean createAccount(Account account) {
	    try {
	        em.merge(account);
	        return true;
        } catch (Exception e) {
	        e.printStackTrace();
        }
        return false;
    }

    @Override
    public Account read(Account account) {
        Account ac = (Account) em.createQuery("from Account where id = "+account.getId()).getSingleResult();
        return ac;
    }

	@Override
	public Account readByAccountName(Account account) {
		Account ac;
		try {
			ac = (Account) em.createQuery("from Account where accountName LIKE '" + account.getAccountName() + "'")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		return ac;
	}

	@Override
	public Account readByAccountName(String accountName) {
		Account ac;
		try {
			ac = (Account) em.createQuery("from Account where accountName LIKE '" + accountName + "'")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		return ac;
	}

	@Override
    public List<Account> readAll() {
        return em.createQuery("from Account").getResultList();
    }

    @Override
    public boolean delete(Account account) throws DAOException {
    	System.out.println("AccountDaoImpl.delete()");
    	account = read(account);
    	//account.setAvatar(null);
    	account.setRoles(null);
    	return super.delete(account);
    	
    }

    @Override
    public boolean deleteAccount(Account account) {
        try {
            delete(account);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

	@Override
	public boolean setImage(Long accId, InputStream is) throws IOException {
		Account account = read(new Account(accId));
		byte [] bytes = IOUtils.toByteArray(is);
		is.close();
		account.setAvatar(bytes);
		update(account);
		return true;
	}

	public byte[] getImage(int accId) {
		Account account = em.find(Account.class, new Long(accId));
		return account.getAvatar();
	}

	@Override
	public void setDataSource(DataSource dataSource) {
		// TODO: To be implemented
	}

	@Override
	public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
		// TODO: To be implemented
	}

	@Override
	public Account getAccountById(Long id) {
        Account ac = (Account) em.createQuery("from Account where id = "+id).getSingleResult();
        return ac;
	}

	@Override
	public List<Account> getAllAccounts() {
        return readAll();
	}

	@Override
	public boolean delete(Long id) {
 		return deleteAccount(getAccountById(id));
	}

	@Override
	public byte[] getImage(Long accId) {
		// TODO:
		return null;
	}

}