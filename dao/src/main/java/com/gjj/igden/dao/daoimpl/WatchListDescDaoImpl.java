package com.gjj.igden.dao.daoimpl;

import java.util.List;
import java.util.ArrayList;

import javax.transaction.Transactional;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class WatchListDescDaoImpl extends AbstractDAO<IWatchListDesc> implements WatchListDescDao {

	public IWatchListDesc read(IWatchListDesc obj) {
		WatchListDesc wl = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where wl.watchListDescKey.watchListId ="+obj.getWatchListDescKey().getWatchListId()).getSingleResult();
		System.out.println("WatchListDescDaoImpl.read():::::"+wl.getWatchListId());
		return wl;
	}
	
	public IWatchListDesc read(Long id, Long accId) {
		/*WatchListDesc watchListDesc = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where"
								+ " wl.watchListDescKey.watchListId="+id).getSingleResult();
		return watchListDesc;*/
		
		WatchListDescKey key = new WatchListDescKey();
		key.setWatchListId(id);
		key.setAccountId(accId);
		return em.find(WatchListDesc.class, key);
	}


	public IWatchListDesc read(WatchListDescKey key) {
	    /*
		WatchListDesc watchListDesc = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where"
				+ " wl.watchListDescKey.watchListId="+id).getSingleResult();
				*/
		return em.find(WatchListDesc.class, key);
	}

	@Override
	public IWatchListDesc read(Long id) {
		WatchListDesc watchListDesc = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where"
								+ " wl.watchListDescKey.watchListId="+id).getSingleResult();
		return watchListDesc;
	}

	public boolean deleteWatchList(IWatchListDesc obj) throws DAOException {
		em.createQuery("DELETE FROM WatchListDesc wl WHERE "
				+ "wl.watchListDescKey.watchListId ="
				+obj.getWatchListDescKey().getWatchListId()).executeUpdate();
		return true;
	}
	
	public IWatchListDesc readByWatchListIdAndAccountId(Long watchListId, Long accountId) {
		WatchListDesc wl = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where wl.watchListDescKey.watchListId ="
							+watchListId+" AND wl.watchListDescKey.accountId = "+accountId).getSingleResult();
		System.out.println("WatchListDescDaoImpl.readByWatchListIdAndAccountId():::"+wl.getWatchListId());
		return wl;
	}

	@Override
	public List<IWatchListDesc> readAll() {
		return em.createQuery("FROM WatchListDesc").getResultList();
	}
	
	public boolean createWatchListDesc(WatchListDesc watchListDesc) {
		try {
			Long max = getMaxIdFromDatabase();
			watchListDesc.getWatchListDescKey().setWatchListId(max + 1);
			super.create(watchListDesc);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<IWatchListDesc> getDataSetsAttachedToAcc(Long id) {
		return  em.createQuery("FROM WatchListDesc WHERE account_fk_id = "+id).getResultList();

	}

	@Override
	public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {

	}

	public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
		try {
			return (IWatchListDesc) em.createQuery("FROM WatchListDesc WHERE watchListDescKey.watchListId = " + dsId + " AND account.id = " + accId).getSingleResult();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return null;
	}

    @Override
    public List<String> getAllStockSymbols(Long dsId, Long accId) {
        return read(dsId, accId).getStockSymbolsList();
    }

    @Override
	public boolean addTicker(Long accId, Long watchlistId, String instId) {
		try {
			InstIdKey key = new InstIdKey();
			key.setAccountId(accId);
			key.setWatchListDescId(watchlistId);
			key.setId(instId);

			InstId inst = new InstId();
			inst.setAccountId(accId);
			inst.setInstId(instId);
			inst.setWatchListDescId(watchlistId);

			em.persist(inst);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteWatchListDesc(Long dsId, Long accId) {
		try {

			IWatchListDesc watchListDesc = getWatchListDesc(dsId, accId);
			em.remove(watchListDesc);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteWatchListDesc(IWatchListDesc watchListDesc) {
		try {
			deleteWatchList(watchListDesc);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean createWatchListDesc(IWatchListDesc watchListDesc) {
		try {
			if(watchListDesc.getWatchListId() == null) {
				em.persist(watchListDesc);
			} else {
				em.merge(watchListDesc);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateWatchListDesc(IWatchListDesc watchListDesc) {
		super.update(watchListDesc);
		return false;
	}
	
	public Long getMaxIdFromDatabase() {
		Long max = (Long) em.createQuery("select MAX(wl.watchListDescKey.watchListId) FROM WatchListDesc wl").getSingleResult();
		System.out.println("WatchListDescDaoImpl.getMaxIdFromDatabase()"+max);
		return null == max ? 0 : max;
	}

}
