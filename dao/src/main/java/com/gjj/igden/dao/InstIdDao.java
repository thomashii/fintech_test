package com.gjj.igden.dao;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.InstId;
import com.gjj.igden.model.InstIdKey;

import java.util.List;

public interface InstIdDao {
    void create(InstId instId) throws DAOException;

    InstId read(InstId obj);

    InstId read(InstIdKey idKey);

    List<InstId> readAll();

    boolean delete(InstId instId) throws DAOException;

    void delete(String instId);

    void delete(Long watchListId, Long accountId);

    void updateInstIdToNull(long watchListId_fk);

    List<String> getAllStockSymbols(IWatchListDesc watchListDesc);

    List<String> searchTickersByChars(String tickerNamePart);
}
