package com.gjj.igden.dao.daoimpl;

import java.util.List;

import com.gjj.igden.dao.InstIdDao;
import com.gjj.igden.model.IWatchListDesc;
import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.InstId;
import com.gjj.igden.model.InstIdKey;

@Repository
@SuppressWarnings("unchecked")
public class InstIdDaoImpl extends AbstractDAO<InstId> implements InstIdDao {

	@Override
	public void create(InstId instId) throws DAOException {
		super.create(instId);
	}
	@Override
	public InstId read(InstId obj) {
		//return (InstId) em.createQuery("from InstId where instId = '"+obj.getInstIdKey().getId()+"'").getSingleResult();
		return read(obj.getInstIdKey());
	}

	@Override
	public InstId read(InstIdKey idKey) {
		return em.find(InstId.class, idKey);
	}
	
	@Override
	public List<InstId> readAll() {
		return em.createQuery("from InstId").getResultList();
	}
	
	@Override
	public boolean delete(InstId instId) throws DAOException {
		em.remove(instId);
		return true;
	}
	
	@Override
	public void delete(String instId) {
		em.createQuery("delete from InstId where instId='"+instId+"'");
	}
	
	@Override
	public void delete(Long watchListId, Long accountId) {
		em.createNativeQuery("DELETE FROM wl_tickers WHERE account_fk_Id="+accountId+" and watchlist_id="+watchListId).executeUpdate();
	}
	
	@Override
	public void updateInstIdToNull(long watchListId_fk) {
		em.createQuery("update InstId set iWatchListDesc = 0 where watchlist_id_fk="+watchListId_fk).executeUpdate();
	}
	
	@Override
	public List<String> getAllStockSymbols(IWatchListDesc watchListDesc) {
		Long id = watchListDesc.getId();
		List<String> list = em.createNativeQuery("SELECT inst_id FROM wl_tickers WHERE watchlist_id = "+ id).getResultList();
		list.forEach(System.out::println);
		return list;
	}
	
	@Override
	public List<String> searchTickersByChars(String tickerNamePart) {
		System.out.println("InstIdDaoImpl.searchTickersByChars():::::"+tickerNamePart);
		List<String> instIdList = em.createNativeQuery("Select inst_id FROM wl_tickers i where inst_id LIKE '%"+tickerNamePart+"%'").getResultList();
		return instIdList;
	}
}
