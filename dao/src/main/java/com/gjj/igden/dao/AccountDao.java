package com.gjj.igden.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.sql.DataSource;

import com.gjj.igden.dao.daoUtil.DAOException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.gjj.igden.model.Account;

public interface AccountDao {
  void setDataSource(DataSource dataSource);

  void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd);

  List<Account> getAllAccounts();

  boolean delete(Account account) throws DAOException;

  boolean delete(Long id);

  Account getAccountById(Long id);

  void update(Account acc);

  boolean createAccount(Account account);

  boolean deleteAccount(Account account);

  void create(Account account) throws DAOException;

  boolean setImage(Long accId, InputStream is) throws IOException;

  byte[] getImage(Long accId);

  Account readByAccountName(Account account);

  Account readByAccountName(String accountName);

  List<Account> readAll();

  Account read(Account account);
}
